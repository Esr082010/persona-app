
import {LitElement, html} from 'lit-element';

class PersonaApp extends LitElement{

    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h1>Persona App</h1>
        `;

    }
}

customElements.define('persona-app', PersonaApp)
